UberW
=====

Overview
--------
UberW is a web view wrapper for using Uber without exposing your device.

Features
--------
- Clears private data on close
- Blocks access to Google trackers and other third-party resources
- Restricts all network requests to HTTPS
- Allows toggling of location permission

Downsides
---------
Navigation is not available, only turn-by-turn direction list

Credits
-------
- Diego Sanguinetti for the Spanish fastlane metadata
- R Raj for the sharing intent support
- Icons: Google/Android/AOSP, License: Apache 2.0, https://google.github.io/material-design-icons/

Donate
-------
- https://divested.dev/donate
